#!/usr//bin/python3
class Log:
    error = 0x10
    info = 0x100
    verbose = 0x1000

class Ftrace:
    tracing_top = '/sys/kernel/debug/tracing'
    current_tracer = '{}/{}'.format(tracing_top, 'current_tracer')
    trace = '{}/{}'.format(tracing_top, 'trace')
    trace_pipe = '{}/{}'.format(tracing_top, 'trace_pipe')
    trace = '{}/{}'.format(tracing_top, 'trace')
    tracing_on = '{}/{}'.format(tracing_top, 'tracing_on')
    set_ftrace_filter = '{}/{}'.format(tracing_top, 'set_ftrace_filter')
    function_profile_enabled = '{}/{}'.format(tracing_top, 'function_profile_enabled')

    trace_stat = '/sys/kernel/debug/tracing/trace_stat'

    def ftrace_enable(self, i):
        cmd = 'sysctl kernel.ftrace_enable=' + str(i)
        return self.run(cmd)

    def run(self, cmd):
        p = subprocess.Popen(cmd.split(), stdout=subprocess.PIPE, stderr=subprocess.PIPE)
        out,err = p.communicate()
        return out, err

    def echo(self, pfile, text):
        ''' This simulate the action of 'echo' in bash '''
        with open(pfile, 'w') as fd:

            # Do not forget the CR
            ret = fd.write(text + '\n')
        return ret

    def function_count(self, funcfilter = None, period = None, dbg=Log.info):
        ''' Out the dictionary per CPU '''

        self.ftrace_enable(1)

        if dbg >= Log.info:
            print(funcfilter)

        if  funcfilter:
            self.echo(self.set_ftrace_filter, funcfilter)

        self.echo(self.function_profile_enabled, '1')

        from pathlib import Path
        p = Path(self.trace_stat)
        funcs = list(p.glob('func*'))

        if dbg >= Log.verbose:
            print(funcs)

        if period:
            if dbg >= Log.info:
                print('Sleep for {}'.format(period))

            from time import sleep
            sleep(int(period))

        d = {}
        header = None
        sep = None
        for f in funcs:
            with f.open('r') as fd:
                ctx = fd.readlines()
                if dbg >= Log.verbose:
                    print("Read {}..".format(f))

                # Convert the functionX to CpuX diretory
                if len(ctx) > 2:
                    header, sep = ctx[0:2]
                    d[ 'cpu' + str(f).split('/')[-1][-1] ] = ctx[2:]
                else:
                    print("No data in {}".format(f))

        ## Capture done, turn off the profiling
        self.echo(self.function_profile_enabled, '0')


        if dbg >= Log.verbose:
            for cpu in d:
                for i in d[cpu]:
                    print(i, end='')
        import pandas as pd
        for i in d:
            d[i] = [ x.strip(' ').split() for x in d[i] ]

            # Remove the 'us' column
            df = pd.DataFrame(d[i])[[0, 1, 2, 4, 6]]

            # the time unit is us
            df.columns = 'Function Hit Time Avg s^2'.split()

            df.set_index('Function', inplace=True)

            df = self.to_numeric(df)
            d[i] = df
        
        self.ftrace_enable(0)
        return d

    def to_numeric(self, df):
        import pandas as pd
        t = pd.DataFrame()
        for c in df:
            t[c] = pd.to_numeric(df[c])
        return t

    def function_count_raw(self, funcfilter = None, period = None):
        ''' Out the dictionary per CPU '''

        self.ftrace_enable(1)

        print(funcfilter)
        if  funcfilter:
            self.echo(self.set_ftrace_filter, funcfilter)

        self.echo(self.function_profile_enabled, '1')

        from pathlib import Path
        p = Path(self.trace_stat)
        funcs = list(p.glob('func*'))

        if period:
            print('Sleep for {}'.format(period))
            from time import sleep
            sleep(int(period))

        d = {}
        for f in funcs:
            with f.open('r') as fd:
                while True:
                    r = fd.readline()
                    if not r:
                        break
                    print(r, end="")
        
        self.ftrace_enable(0)
        return d


    def fcount(self, func_filter = None, period = None, sort = 'Hit', num = None, dbg = Log.info):

        d = self.function_count(func_filter, period, dbg)

        import pandas as pd
        all_cpus = pd.concat(d.values())

        result = self.sort(all_cpus, sort, ascending = False)

        if dbg >= Log.info:
            if num:
                print(result.iloc[0:int(num)])
            else:
                print(result.to_string())
        return result


    def sort(self, df, sort='Hit', ascending=False):
        try:
            return df.sort_values(by = sort, ascending = False)
        except:
            print("Key error, not sorted")
            return df

        

if __name__ == '__main__':
    from optparse import OptionParser
    import subprocess

    parser = OptionParser(usage="usage: $prog [options]")

    parser.add_option("-l", "--force", dest="set_ftrace_filter", help="set_ftrace_filter", 
            metavar="FUCNTION FILTER")
    parser.add_option("-p", "--period", dest="period", help="The time for Ftrace to run before we get results", 
            metavar="PERIOD")
    parser.add_option("-s", "--sort", dest="sort", help="Sort by column", default='Hit',
            metavar="SORT")

    parser.add_option("-n", "--num", dest="num", help="Number to show", 
            metavar="NUM")

    (opt, args) = parser.parse_args()

    #Ftrace().funccount(opt.set_ftrace_filter, period = opt.period)
    Ftrace().fcount(opt.set_ftrace_filter, period = opt.period, sort = opt.sort, num = opt.num)
